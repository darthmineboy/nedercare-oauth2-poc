package nl.nedercare.oauth2poc.util;

import java.util.Set;

public final class ScopeUtil {
    private ScopeUtil() {
    }

    public static boolean containsAllScopes(Set<String> scopeGranted, Set<String> scopeRequested) {
        return scopeRequested.stream().allMatch(scope -> containsScope(scopeGranted, scope));
    }

    public static boolean containsScope(Set<String> scopeGranted, String scopeRequested) {
        return scopeGranted.stream().anyMatch(originalScope -> originalScope.equals(scopeRequested) || scopeRequested.matches(originalScope.replace("*", ".*?")));
    }
}
