package nl.nedercare.oauth2poc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

@Entity
@Table(name = "USER_", uniqueConstraints = {@UniqueConstraint(columnNames = {"USER_NAME"})})
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class User implements UserDetails, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USER_NAME")
    private String username;

    @JsonIgnore
    @Column(name = "PASSWORD")
    private String password;

    @JsonIgnore
    @Column(name = "ACCOUNT_EXPIRED")
    private boolean accountExpired;

    @JsonIgnore
    @Column(name = "ACCOUNT_LOCKED")
    private boolean accountLocked;

    @JsonIgnore
    @Column(name = "CREDENTIALS_EXPIRED")
    private boolean credentialsExpired;

    @JsonIgnore
    @Column(name = "ENABLED")
    private boolean enabled;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return !isAccountExpired();
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return !isAccountLocked();
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return !isCredentialsExpired();
    }
}
