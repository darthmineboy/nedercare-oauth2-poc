package nl.nedercare.oauth2poc.repository;

import nl.nedercare.oauth2poc.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT DISTINCT user FROM User user " +
            "WHERE user.username = :username")
    User findByUsername(@Param("username") String username);
}
