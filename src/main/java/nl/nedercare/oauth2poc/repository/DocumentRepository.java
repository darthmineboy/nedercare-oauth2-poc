package nl.nedercare.oauth2poc.repository;

import nl.nedercare.oauth2poc.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

    @Query("FROM Document WHERE owner_id = :userId")
    List<Document> findDocumentsByUser(@Param("userId") Long userId);

}
