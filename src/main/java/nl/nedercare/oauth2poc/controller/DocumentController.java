package nl.nedercare.oauth2poc.controller;

import nl.nedercare.oauth2poc.model.Document;
import nl.nedercare.oauth2poc.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users/{userId}/documents")
public class DocumentController {

    @Autowired
    private DocumentRepository documentRepository;

    @PreAuthorize("#oauth2.hasAnyScope('users/*', 'users/' + #userId)")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    List<Document> getAll(@PathVariable("userId") Long userId) {
        return documentRepository.findDocumentsByUser(userId);
    }

    @PreAuthorize("#oauth2.hasAnyScope('users/*', 'users/' + #userId)")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Document get(@PathVariable("userId") Long userId, @PathVariable Long id) {
        return documentRepository.findOne(id);
    }

}
