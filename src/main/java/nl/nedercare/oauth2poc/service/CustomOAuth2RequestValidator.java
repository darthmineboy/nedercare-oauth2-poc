package nl.nedercare.oauth2poc.service;

import org.springframework.security.oauth2.common.exceptions.InvalidScopeException;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.OAuth2RequestValidator;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestValidator;

import java.util.Iterator;
import java.util.Set;

import static nl.nedercare.oauth2poc.util.ScopeUtil.containsAllScopes;

/**
 * Support wildcard scope.
 * <p>
 * Based on {@link DefaultOAuth2RequestValidator}
 */
public class CustomOAuth2RequestValidator implements OAuth2RequestValidator {
    public CustomOAuth2RequestValidator() {
    }

    public void validateScope(AuthorizationRequest authorizationRequest, ClientDetails client) throws InvalidScopeException {
        this.validateScope(authorizationRequest.getScope(), client.getScope());
    }

    public void validateScope(TokenRequest tokenRequest, ClientDetails client) throws InvalidScopeException {
        this.validateScope(tokenRequest.getScope(), client.getScope());
    }

    private void validateScope(Set<String> requestScopes, Set<String> clientScopes) {
        if (!containsAllScopes(clientScopes, requestScopes)) {
            if (clientScopes != null && !clientScopes.isEmpty()) {
                Iterator var3 = requestScopes.iterator();

                while (var3.hasNext()) {
                    String scope = (String) var3.next();
                    if (!clientScopes.contains(scope)) {
                        throw new InvalidScopeException("Invalid scope: " + scope, clientScopes);
                    }
                }
            }
        }

        if (requestScopes.isEmpty()) {
            throw new InvalidScopeException("Empty scope (either the client or the user is not allowed the requested scopes)");
        }
    }
}
