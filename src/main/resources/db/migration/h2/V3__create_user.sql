CREATE TABLE USER_
(
  ID                  BIGINT GENERATED BY DEFAULT AS IDENTITY,
  PASSWORD            VARCHAR(255),
  USER_NAME           VARCHAR(255),
  ACCOUNT_EXPIRED     BOOLEAN,
  ACCOUNT_LOCKED      BOOLEAN,
  CREDENTIALS_EXPIRED BOOLEAN,
  ENABLED             BOOLEAN,
  PRIMARY KEY (ID)
);

ALTER TABLE USER_
  ADD CONSTRAINT USER_USER_NAME UNIQUE (USER_NAME);

