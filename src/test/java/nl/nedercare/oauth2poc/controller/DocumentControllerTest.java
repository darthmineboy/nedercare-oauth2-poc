package nl.nedercare.oauth2poc.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.nedercare.oauth2poc.Application;
import nl.nedercare.oauth2poc.model.Document;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class DocumentControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getAll() throws Exception {
        MvcResult tokenResult = mvc.perform(post("/oauth/token")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .param("grant_type", "password")
                .param("username", "externalApp")
                .param("password", "password1234")
                .param("client_id", "spring-security-oauth2-read-write-client")
                .param("client_secret", "spring-security-oauth2-read-write-client-password1234")
                .param("scope", "users/1")
        )
                .andExpect(status().isOk())
                .andReturn();
        OAuth2AccessToken accessToken = objectMapper.readValue(tokenResult.getResponse().getContentAsString(), OAuth2AccessToken.class);

        MvcResult documentsResult = mvc.perform(get("/api/users/1/documents")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, String.format("%s %s", accessToken.getTokenType(), accessToken.getValue())))
                .andExpect(status().isOk())
                .andReturn();

        List<Document> documents = objectMapper.readValue(documentsResult.getResponse().getContentAsString(), new TypeReference<List<Document>>() {
        });
        Assert.assertEquals(2, documents.size());
    }

    @Test
    public void getAll_invalidAccessToken() throws Exception {
        mvc.perform(get("/api/users/1/documents")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "bearer thisCantBeAValidToken"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void getAll_missingAuthorizationHeader() throws Exception {
        mvc.perform(get("/api/users/1/documents")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    /**
     * Requests scope 'user/2' but requests data of different user 2.
     *
     * @throws Exception
     */
    @Test
    public void getAll_differentUserScopeThanRequired() throws Exception {
        MvcResult tokenResult = mvc.perform(post("/oauth/token")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .param("grant_type", "password")
                .param("username", "externalApp")
                .param("password", "password1234")
                .param("client_id", "spring-security-oauth2-read-write-client")
                .param("client_secret", "spring-security-oauth2-read-write-client-password1234")
                .param("scope", "users/2")
        ).andExpect(status().isOk())
                .andReturn();
        OAuth2AccessToken accessToken = objectMapper.readValue(tokenResult.getResponse().getContentAsString(), OAuth2AccessToken.class);

        mvc.perform(get("/api/users/1/documents")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, String.format("%s %s", accessToken.getTokenType(), accessToken.getValue())))
                .andExpect(status().isForbidden())
                .andReturn();
    }

}