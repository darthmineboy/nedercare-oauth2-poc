package nl.nedercare.oauth2poc.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.nedercare.oauth2poc.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class OAuth2TokenTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Test
    public void passwordGrant() throws Exception {
        final String requestScope = "users/*";
        MvcResult tokenResult = mvc.perform(post("/oauth/token")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .param("grant_type", "password")
                .param("username", "externalApp")
                .param("password", "password1234")
                .param("client_id", "spring-security-oauth2-read-write-client")
                .param("client_secret", "spring-security-oauth2-read-write-client-password1234")
                .param("scope", requestScope)
        ).andExpect(status().isOk()).andReturn();

        OAuth2AccessToken accessToken = objectMapper.readValue(tokenResult.getResponse().getContentAsString(), OAuth2AccessToken.class);
        assertEquals(requestScope, accessToken.getScope().stream().collect(Collectors.joining()));
    }

    @Test
    public void passwordGrant_invalidClient() throws Exception {
        final String requestScope = "users/*";
        MvcResult tokenResult = mvc.perform(post("/oauth/token")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .param("grant_type", "password")
                .param("username", "externalApp")
                .param("password", "password1234")
                .param("client_id", "spring-security-oauth2-read-write-client")
                .param("client_secret", "invalidClientPassword")
                .param("scope", requestScope)
        ).andExpect(status().isUnauthorized()).andReturn();
    }

    @Test
    public void refreshTokenGrant_downscope() throws Exception {
        final String requestScope = "users/*";
        MvcResult tokenResult = mvc.perform(post("/oauth/token")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .param("grant_type", "password")
                .param("username", "externalApp")
                .param("password", "password1234")
                .param("client_id", "spring-security-oauth2-read-write-client")
                .param("client_secret", "spring-security-oauth2-read-write-client-password1234")
                .param("scope", requestScope)
        ).andExpect(status().isOk()).andReturn();

        OAuth2AccessToken accessToken = objectMapper.readValue(tokenResult.getResponse().getContentAsString(), OAuth2AccessToken.class);
        assertEquals(requestScope, accessToken.getScope().stream().collect(Collectors.joining()));

        final String requestDownscope = "users/1";
        MvcResult refreshTokenResult = mvc.perform(post("/oauth/token")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .param("grant_type", "refresh_token")
                .param("refresh_token", accessToken.getRefreshToken().getValue())
                .param("client_id", "spring-security-oauth2-read-write-client")
                .param("client_secret", "spring-security-oauth2-read-write-client-password1234")
                .param("scope", requestDownscope)
        ).andExpect(status().isOk()).andReturn();

        OAuth2AccessToken downscopeAccessToken = objectMapper.readValue(refreshTokenResult.getResponse().getContentAsString(), OAuth2AccessToken.class);
        assertEquals(requestDownscope, downscopeAccessToken.getScope().stream().collect(Collectors.joining()));
    }

}
