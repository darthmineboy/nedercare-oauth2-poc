package nl.nedercare.oauth2poc.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ScopeUtilTest {

    @Test
    public void containsAllScopes() {
        Set<String> originalScopes = Stream.of("a", "b").collect(Collectors.toSet());
        Set<String> scopes = Stream.of("a", "b").collect(Collectors.toSet());
        Assert.assertTrue(ScopeUtil.containsAllScopes(originalScopes, scopes));
    }

    @Test
    public void containsAllScopes_false() {
        Set<String> originalScopes = Stream.of("a", "b").collect(Collectors.toSet());
        Set<String> scopes = Stream.of("a", "b", "c").collect(Collectors.toSet());
        Assert.assertFalse(ScopeUtil.containsAllScopes(originalScopes, scopes));
    }

    @Test
    public void containsAllScopes_wildcard() {
        Set<String> originalScopes = Stream.of("a", "b/*").collect(Collectors.toSet());
        Set<String> scopes = Stream.of("a", "b/1").collect(Collectors.toSet());
        Assert.assertTrue(ScopeUtil.containsAllScopes(originalScopes, scopes));
    }


}