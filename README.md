# Proof of concept OAuth 2
Dit is een POC van het downscopen van OAuth 2 tokens met Spring security, met als basis dit [project](https://dzone.com/articles/secure-spring-rest-with-spring-security-and-oauth2). 
Spring security heeft een implementatie voor OAuth 2, die voor dit POC is gebruikt. 

Met downscopen wordt bedoeld dat van de bestaande scope er een deel van de scope wordt weggehaald, 
bijvoorbeeld: "read write" wordt gedownscoped naar "read". 
Spring security OAuth 2 ondersteund het downscopen door scopes weg te halen, maar ondersteund niet het principe van wildcards, i.e. 'users/\*''.
Bij wildcards kan door middel van een asterix worden aangegeven dat dit te vervangen door elke waarde is. 
Bijvoorbeeld de scopes 'users/1' en users/2' vallen allebei binnen de wildcard scope 'users/\*'. 
Aan dit POC is ondersteuning voor wildcard scopes toegevoegd zodat de scope 'users/\*' gedownscoped kan worden naar bijvoorbeeld 'users/1' of 'users/2'.

Voor de test cases zie de ```src/main/test``` folder.

# Database
Dit POC maakt gebruik van een in memory H2 database.
Deze database wordt automatisch aangemaakt.
De [H2 database console](http://localhost:8080/console) kan in de browser bezocht worden.
Om in te loggen moet de JDBC url veranderd worden naar ```jdbc:h2:mem:testdb```.

# Gebruikerswachtwoorden
Wachtwoord hashes voor gebruikers kunnen gegenereerd worden via deze [tool](https://www.browserling.com/tools/bcrypt) met 8 rounds.

# Bronnen
https://github.com/cloudfoundry/uaa/issues/325
https://tools.ietf.org/html/rfc6749#section-6
https://openid.net/specs/openid-heart-fhir-oauth2-1_0-2017-05-31.html

# Todo
Authentication
https://www.baeldung.com/securing-a-restful-web-service-with-spring-security